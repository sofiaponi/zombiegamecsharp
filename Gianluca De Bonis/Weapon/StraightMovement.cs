﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;

namespace Weapon
{
    public class StraightMovement : IMovement
    {
        private double STANDARD_VELOCITY = 5;

        private double velocity;
        private double angle;
        private Point position;
        private Point positionVariation;

        public StraightMovement(Point from, Point towards)
        {
            this.position = from;
            this.velocity = STANDARD_VELOCITY;
            this.CalculateStep(from, towards);
        }

        private void CalculateStep(Point from, Point towards)
        {
            double xStep;
            double yStep;
            double distanceX = towards.X - from.X;
            double distanceY = towards.Y - from.Y;
            this.angle = Math.Atan2(distanceY, distanceX);
            xStep = Math.Cos(this.angle) * this.velocity;
            yStep = Math.Sin(this.angle) * this.velocity;
            this.positionVariation = new Point((int)xStep, (int)yStep);
        }

        Point IMovement.GetActualPosition()
        {
            return this.position;
        }

        double IMovement.GetAngle()
        {
            return this.angle;
        }

        double IMovement.GetVelocity()
        {
            return this.velocity;
        }

        bool IMovement.HasEnded()
        {
            return false;
        }

        public void SetVelocity(double velocityFactor)
        {
            this.velocity = velocityFactor;
        }

        void IMovement.Update()
        {
            position.Add(this.positionVariation);
        }
    }
}
