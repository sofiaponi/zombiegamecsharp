﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weapon
{
    public enum EntityType
    {
        /**
         * Player
         */
        PLAYER,
        /**
         * Zombie.
         */
        ZOMBIE,
        /**
         * Weapon.
         */
        WEAPON,
        /**
         * Bullet.
         */
        BULLET,
        /**
         * 
         */
        MELEE_ATTACK,
        /**
         * Obstacle.
         */
        OBSTACLE
    }
}
