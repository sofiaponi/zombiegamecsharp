﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public abstract class AbstractEntity : IEntity
    {
        private Point position;
        private EntityType type;
        private double width;
        private double height;

        public AbstractEntity(Point p2d, EntityType type)
        {
            this.position = p2d;
            this.type = type;
            this.width = 10;
            this.height = 10;
        }

        public virtual Point Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public EntityType Type
        {
            get
            {
                return this.type;
            }
        }

        public double Width
        {
            get
            {
                return this.width;
            }
        }

        public double Height
        {
            get
            {
                return this.height;
            }
        }
    }
}
