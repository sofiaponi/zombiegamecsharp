﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public enum WeaponType
    {
        SHORT_RANGE,
        LONG_RANGE
    }

    public interface IWeapon: IEntity
    {
        String GetName();

        IAttack Attack(Point towards);

        int GetDamage();

        WeaponType GetWeaponType();

        void Update();
    }
}
