﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public interface IMovement
    {
        Point GetActualPosition();

        double GetAngle();

        void Update();

        double GetVelocity();

        Boolean HasEnded();
    }
}
