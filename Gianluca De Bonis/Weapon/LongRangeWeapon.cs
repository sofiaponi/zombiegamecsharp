﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weapon
{
    public class LongRangeWeapon : AbstractEntity, ILongRangeWeapon
    {
        public new Point Position { get; }

        public new EntityType Type { get; set; }

        public new double Width { get; set; }

        public new double Height { get; set; }

        public String Name { get; }

        private readonly long rechargeTime;
        private readonly int damage;
        private readonly long rateOfFire;
        private readonly int magazineSize;

        private int ammoInMagazine;
        private Boolean isRecharging;
        private long rechargingTime;

        private const WeaponType WEAPON_TYPE = WeaponType.LONG_RANGE;
        private const EntityType ENTITY_TYPE = EntityType.WEAPON;

        public LongRangeWeapon(Point p2d, String name, int damage,
            long rechargeTime, long rateOfFire, int magazineSize) : base(p2d, ENTITY_TYPE)
        {
            this.Name = name;
            this.rechargeTime = rechargeTime;
            this.damage = damage;
            this.rateOfFire = rateOfFire;
            this.magazineSize = magazineSize;

            this.ammoInMagazine = magazineSize;
            this.isRecharging = false;
        }

        public WeaponType GetWeaponType()
        {
            return WEAPON_TYPE;
        }

        public IAttack Attack(Point towards)
        {
            return new Bullet(this.Position, towards, this.damage);
        }

        public int GetActualAmmo()
        {
            return this.ammoInMagazine;
        }

        public int GetDamage()
        {
            return this.damage;
        }

        public string GetName()
        {
            return this.Name;
        }

        public bool IsRecharging()
        {
            return this.isRecharging;
        }

        public void StartRecharging()
        {
            this.isRecharging = true;
            this.rechargingTime = DateTime.Now.Millisecond;
        }

        public void Update()
        {
            if (!isRecharging && this.ammoInMagazine <= 0)
            {
                this.StartRecharging();
            }

            if (isRecharging && DateTime.Now.Millisecond - this.rechargingTime >= rechargeTime)
            {
                this.isRecharging = false;
                this.RechargeMagazine();
            }
        }

        private void RechargeMagazine()
        {
            this.ammoInMagazine = magazineSize;
        }

        public class Builder
        {
            private const long STANDARD_RECHARGE_TIME = 150;
            private const long STANDARD_RATE_OF_FIRE = 300;
            private const int STANDARD_MAGAZINE_SIZE = 10;

            private readonly String name;
            private readonly int damage;
            private Point position;
            private long rechargeTime;
            private long rateOfFire;
            private int magazineSize;

            public Builder(Point position, String name, int damage)
            {
                this.position = position;
                this.name = name;
                this.damage = damage;

                this.rateOfFire = STANDARD_RATE_OF_FIRE;
                this.rechargeTime = STANDARD_RECHARGE_TIME;
                this.magazineSize = STANDARD_MAGAZINE_SIZE;
            }

            public Builder RechargeTime(long duration)
            {
                this.rechargeTime = duration;
                return this;
            }

            public Builder RateOfFire(long duration)
            {
                this.rateOfFire = duration;
                return this;
            }

            public Builder MagazineSize(int magazineSize)
            {
                this.magazineSize = magazineSize;
                return this;
            }

            public LongRangeWeapon Build()
            {
                return new LongRangeWeapon(this.position, this.name, this.damage, this.rechargeTime, this.rateOfFire, this.magazineSize);
            }
        }
    }
}
