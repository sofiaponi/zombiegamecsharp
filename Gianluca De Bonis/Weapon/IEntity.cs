﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public interface IEntity
    {
        Point Position { get; }

        EntityType Type { get; }

        double Width { get; }

        double Height { get; }
    }
}
