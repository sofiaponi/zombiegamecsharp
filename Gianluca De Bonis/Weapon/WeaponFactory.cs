﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public class WeaponFactory
    {
        public enum WeaponName
        {
            GUN,
            RIFLE,
            MACHINE_GUN,
            KNIFE
        }

        private const int GUN_DAMAGE = 25;

        private const int RIFLE_DAMAGE = 50;
        private const long RIFLE_RATE_OF_FIRE = 200;
        private const long RIFLE_RECHARGE = 1500;
        private const int RIFLE_MAGAZINE = 50;

        private const int MACHINEGUN_DAMAGE = 75;
        private const long MACHINEGUN_RATE_OF_FIRE = 50;
        private const long MACHINEGUN_RECHARGE = 4000;
        private const int MACHINEGUN_MAGAZINE = 100;

        public ILongRangeWeapon getLongRangeWeapon(Point position, WeaponName name)
        {
            switch (name)
            {
                case WeaponName.GUN:
                    return this.GetGun(position);
                case WeaponName.RIFLE:
                    return this.GetRifle(position);
                case WeaponName.MACHINE_GUN:
                    return this.GetMachineGun(position);
                default:
                    return this.GetGun(position);
            }
        }

        public IShortRangeWeapon GetShortRangeWeapon(Point position, WeaponName name)
        {
            switch (name)
            {
                case WeaponName.KNIFE:
                    return this.getKnife(position);
                default:
                    return this.getKnife(position);
            }
        }

        private ILongRangeWeapon GetGun(Point position)
        {
            return new LongRangeWeapon.Builder(position, "Gun", GUN_DAMAGE)
                                          .Build();
        }

        private ILongRangeWeapon GetRifle(Point position)
        {
            return new LongRangeWeapon.Builder(position, "Rifle", RIFLE_DAMAGE)
                                      .RechargeTime(RIFLE_RECHARGE)
                                      .RateOfFire(RIFLE_RATE_OF_FIRE)
                                      .MagazineSize(RIFLE_MAGAZINE)
                                      .Build();
        }

        private ILongRangeWeapon GetMachineGun(Point position)
        {
            return new LongRangeWeapon.Builder(position, "Machine Gun", MACHINEGUN_DAMAGE)
                                      .RateOfFire(MACHINEGUN_RATE_OF_FIRE)
                                      .RechargeTime(MACHINEGUN_RECHARGE)
                                      .MagazineSize(MACHINEGUN_MAGAZINE)
                                      .Build();
        }

        private IShortRangeWeapon getKnife(Point position)
        {
            return new Knife(position);
        }
    }
}
