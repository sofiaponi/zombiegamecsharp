﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weapon
{
    public class AngularMovement : IMovement
    {
        private double STANDARD_VELOCITY = 20;

        private double RAD_TOTAL_ANGLE = 2.1; // 120°
        private Point pivot;
        private Point position;
        private double angle;
        private double da;
        private double angularLength;
        private double end;
        private double height;
        private Boolean isOver;
        private double frequency;

        public AngularMovement(Point from, Point towards, double height)
        {
            this.pivot = from;
            this.height = height;
            this.angularLength = RAD_TOTAL_ANGLE;
            this.isOver = false;
            this.SetFrequency(STANDARD_VELOCITY);
            this.CalculateStep(from, towards);
        }
        private void CalculateStep(Point from, Point towards)
        {
            double distanceX = towards.X - from.X;
            double distanceY = towards.Y - from.Y;
            this.angle = Math.Atan2(distanceY, distanceX) - this.angularLength / 2;
            this.da = this.angularLength * this.frequency;
            this.end = this.angle + this.angularLength;
            this.position = this.pivot.Add(new Point((int)(height * Math.Cos(angle)), (int)(height * Math.Sin(angle))));
        }

        public void Update()
        {
            if (this.angle < this.end)
            {
                this.angle += da;
                this.position = this.pivot.Add(new Point((int)(height * Math.Cos(angle)), (int)(height * Math.Sin(angle))));
            }
            else
            {
                this.isOver = true;
            }
        }

        public Point GetActualPosition()
        {
            return this.position;
        }

        public double GetAngle()
        {
            return this.angle;
        }

        public double GetVelocity()
        {
            return this.frequency;
        }

        public bool HasEnded()
        {
            return this.isOver;
        }

        private void SetFrequency(double steps)
        {
            this.frequency = 1 / steps;
        }
    }
}
