﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public class KnifeAttack: AbstractAttack
    {

        private const double HEIGHT = 30;
        private const double WIDTH = 10;
        private readonly IMovement trajectory;

        public KnifeAttack(Point from, Point towards, int damage): base(from, damage, EntityType.MELEE_ATTACK)
        {
            this.trajectory = new AngularMovement(from, towards, HEIGHT);
        }

        protected override IMovement GetMovement()
        {
            return this.trajectory;
        }
    }
}
