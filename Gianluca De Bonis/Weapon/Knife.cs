﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weapon
{
    public class Knife : AbstractEntity, IShortRangeWeapon
    {
        public new Point Position { get; }

        public new EntityType Type { get; set; }

        public new double Width { get; set; }

        public new double Height { get; set; }

        public String Name { get; }

        private static int DAMAGE = 1000;
        private static long ATTACK_RATE = 1000;
        private Boolean canAttack;
        private long lastAttack;

        private const WeaponType WEAPON_TYPE = WeaponType.LONG_RANGE;
        private const EntityType ENTITY_TYPE = EntityType.WEAPON;


        public Knife(Point p2d): base(p2d, ENTITY_TYPE)
        {
            this.canAttack = true;
        }

        public IAttack Attack(Point towards)
        {
            if (canAttack)
            {
                this.canAttack = false;
                this.lastAttack = DateTime.Now.Millisecond;
                return new KnifeAttack(this.Position, towards, DAMAGE);
            } else
            {
                return null;
            }
        }

        public int GetDamage()
        {
            return DAMAGE;
        }

        public string GetName()
        {
            return this.Name;
        }

        public WeaponType GetWeaponType()
        {
            return WEAPON_TYPE;
        }

        public void Update()
        {
            if (!canAttack && DateTime.Now.Millisecond - this.lastAttack > ATTACK_RATE)
            {
                canAttack = true;
            }
        }
    }
}
