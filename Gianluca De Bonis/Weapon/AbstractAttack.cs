﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public abstract class AbstractAttack : AbstractEntity, IAttack
    {
        private int damage;

        public AbstractAttack(Point p2d, int damage, EntityType type): base(p2d, type)
        {
            this.damage = damage;
        }

        protected abstract IMovement GetMovement();

        public int GetDamage()
        {
            return this.damage;
        }

        public Point GetPosition()
        {
            return GetMovement().GetActualPosition();
        }

        public void Update()
        {
            this.GetMovement().Update();
        }

        public double GetDirection()
        {
            return this.GetMovement().GetAngle();
        }

        public Boolean HasEnded()
        {
            return this.GetMovement().HasEnded();
        }

        public Point GetVelocity()
        {
            double vel = this.GetMovement().GetVelocity();
            return new Point((int)(vel * Math.Cos(this.GetDirection())), (int)(vel * Math.Sin(this.GetDirection())));
        }

        /**
         * Not useful for this ActiveEntity. It will have no effect.
         */
        public void SetVelocity(Point vel)
        {

        }

        /**
         * Not useful for this {@link ActiveEntity}. It will have no effect.
         */
        public void SetDirection(double angle)
        {

        }
    }
}
