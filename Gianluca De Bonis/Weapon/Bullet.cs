﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Weapon
{
    public class Bullet : AbstractAttack
    {
        private const double HEIGHT = 10;
        private const double WIDTH = 7;
        private readonly IMovement trajectory;

        public Bullet(Point from, Point towards, int damage): base(from, damage, EntityType.BULLET)
        {
            this.trajectory = new StraightMovement(from, towards);
        }

        protected override IMovement GetMovement()
        {
            return this.trajectory;
        }
    }
}
