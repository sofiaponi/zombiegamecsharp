﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

namespace Weapon.Tests
{
    [TestClass()]
    public class WeaponFactoryTests
    {
        WeaponFactory factory = new WeaponFactory();
        Point initialPoint = new Point(1, 1);

        [TestMethod()]
        public void getLongRangeWeaponTest()
        {
            ILongRangeWeapon w = factory.getLongRangeWeapon(initialPoint, WeaponFactory.WeaponName.GUN);
            Assert.AreEqual(w.GetName(), "Gun");
            Assert.IsFalse(w.IsRecharging());
            int ammo = w.GetActualAmmo();
            Bullet a = (Bullet)w.Attack(new Point(2, 2));
            Assert.AreEqual(a.GetType(), Weapon.EntityType.BULLET);
            Assert.AreEqual(a.GetDamage(), w.GetDamage());
            Assert.IsFalse(a.HasEnded());
            double angle = a.GetDirection();
            a.Update();
            a.Update();
            Assert.AreEqual(a.GetDirection(), angle);
            Assert.IsTrue(a.GetPosition() != initialPoint);
            Assert.AreEqual(w.GetActualAmmo(), ammo - 1);
            w.StartRecharging();
            Assert.IsTrue(w.IsRecharging());
            Thread.Sleep(1000);
            Assert.IsFalse(w.IsRecharging());
            Assert.AreEqual(w.GetActualAmmo(), ammo);

            ILongRangeWeapon w2 = factory.getLongRangeWeapon(initialPoint, WeaponFactory.WeaponName.RIFLE);
            Assert.AreEqual(w2.GetName(), "Rifle");
            Assert.AreNotEqual(w2.GetDamage(), w.GetDamage());
            Assert.AreNotEqual(ammo, w2.GetActualAmmo());

            ILongRangeWeapon w3 = factory.getLongRangeWeapon(initialPoint, WeaponFactory.WeaponName.KNIFE);
            Assert.AreEqual(w.GetName(), "Gun"); // Standard weapon if a short range weapon is requested
        }

        [TestMethod()]
        public void GetShortRangeWeaponTest()
        {
            IShortRangeWeapon w = factory.GetShortRangeWeapon(initialPoint, WeaponFactory.WeaponName.KNIFE);
            Assert.AreEqual(w.GetName(), "Knife");
            Assert.AreEqual(w.GetType(), Weapon.EntityType.WEAPON);

            KnifeAttack a = (KnifeAttack)w.Attack(new Point(5, 5));
            Assert.AreEqual(a.GetDamage(), w.GetDamage());
            Assert.AreEqual(a.GetPosition(), initialPoint);
            Assert.AreEqual(a.GetType(), Weapon.EntityType.MELEE_ATTACK);
            double angle = a.GetDirection();
            a.Update();
            a.Update();
            Assert.IsFalse(a.HasEnded());
            Assert.AreEqual(initialPoint, a.GetPosition());
            Assert.AreNotEqual(angle, a.GetDirection());
        }
    }
}