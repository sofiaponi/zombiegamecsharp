﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
    public class Life : ILife
    {
        private int hp;

        public Life(int maxHp)
        {
            this.hp = maxHp;
        }

        public void DecreaseHP(int decrease)
        {
            this.hp -= decrease;
        }

        public int HP
        {
            get
            {
                return this.hp;
            }
        }

        public bool IsAlive()
        {
            return this.hp > 0;
        }
    }
}
