﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
    public class Zombie : AbstractActiveLivingEntity
    {
        private int damageDealt { get; }

        public Zombie(Point startingPos, int velocity, int maxHp, int damageDealt) : base(new Point(velocity, velocity), maxHp, startingPos, EntityType.ZOMBIE)
        {
            this.damageDealt = damageDealt;
        }

        public class Builder
        {
            private Point spawnPoint;
            private int velocity;
            private int maxHp;
            private int damageDealt;

            public Builder(Point spawnPoint)
            {
                this.spawnPoint = spawnPoint;
            }

            public Builder Velocity(int velocity)
            {
                this.velocity = velocity;
                return this;
            }

            public Builder MaxHp(int maxHp)
            {
                this.maxHp = maxHp;
                return this;
            }

            public Builder DamageDealt(int dmg)
            {
                this.damageDealt = dmg;
                return this;
            }

            public Zombie build()
            {
                return new Zombie(spawnPoint, velocity, maxHp, damageDealt);
            }
        }
    }
}
