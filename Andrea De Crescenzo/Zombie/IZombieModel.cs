﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
    public interface IZombieModel
    {

        int ZombiesToSpawn { set; }

        Tiers ZombieTier { set; }

        ISet<Zombie> Zombies { get; }

        int ZombiesCount { get; }

        int KilledZombies { get; }

        void HitZombie(Zombie zombie, int damage);

        void Update();

    }
}
