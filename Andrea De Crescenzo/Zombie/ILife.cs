﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
    public interface ILife
    {
        int HP { get; }

        void DecreaseHP(int decrease);

        bool IsAlive();
    }
}
