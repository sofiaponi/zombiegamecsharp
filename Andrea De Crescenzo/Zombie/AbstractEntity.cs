﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
    public abstract class AbstractEntity : IEntity
    {
        private Point position;
        private EntityType type;
        private double width;
        private double height;

        public AbstractEntity(Point point, EntityType type)
        {
            this.position = point;
            this.type = type;
            this.width = 10;
            this.height = 10;
        }

        public virtual Point Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public EntityType Type
        {
            get
            {
                return this.type;
            }
        }

        public double Width
        {
            get
            {
                return this.width;
            }
        }

        public double Height
        {
            get
            {
                return this.height;
            }
        }
    }
}
