﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
	public class Tiers
	{
		public static readonly Tiers WEAK = new Tiers(InnerEnum.WEAK, 5, 100, 1);

		public static readonly Tiers LOW = new Tiers(InnerEnum.LOW, 5, 120, 2);

		public static readonly Tiers AVERAGE = new Tiers(InnerEnum.AVERAGE, 7, 150, 4);

		public static readonly Tiers STRONG = new Tiers(InnerEnum.STRONG, 8, 180, 6);

		public static readonly Tiers BRUTAL = new Tiers(InnerEnum.BRUTAL, 10, 220, 8);

		private static readonly List<Tiers> valueList = new List<Tiers>();

		static Tiers()
		{
			valueList.Add(WEAK);
			valueList.Add(LOW);
			valueList.Add(AVERAGE);
			valueList.Add(STRONG);
			valueList.Add(BRUTAL);
		}

		public enum InnerEnum
		{
			WEAK,
			LOW,
			AVERAGE,
			STRONG,
			BRUTAL,
		}

		public int velocity { get; }
		public int maxHp { get; }
		public int damageDealt { get; }

		Tiers(InnerEnum innerEnum, int velocity, int maxHp, int damageDealt)
		{
			this.velocity = velocity;
			this.maxHp = maxHp;
			this.damageDealt = damageDealt;
		}
	}
}
