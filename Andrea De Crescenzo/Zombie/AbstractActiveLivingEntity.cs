﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
	public abstract class AbstractActiveLivingEntity : AbstractEntity, IActiveLivingEntity
	{
		private Point vel;
		private readonly ILife lifeManager;
		private double direction;

		public AbstractActiveLivingEntity(Point vel, int maxHp, Point point, EntityType type) : base(point, type)
		{
			this.vel = vel;
			this.lifeManager = new Life(maxHp);
			this.direction = 0.0;
		}

		public double Direction
		{
			set
			{
				this.direction = value;
			}
			get
			{
				return this.direction;
			}
		}

		public Point Velocity
		{
			set
			{
				this.vel = value;
			}
			get
			{
				return this.vel;
			}
		}

		public ILife LifeManager
		{
			get
			{
				return this.lifeManager;
			}
		}

		public virtual void Update()
		{
			Point point = Point.Add(base.Position, new Size(vel));
			base.Position = point;
		}
	}
}
