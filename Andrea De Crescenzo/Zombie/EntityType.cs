﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies
{
	public class EntityType
	{
		public static readonly EntityType PLAYER = new EntityType(InnerEnum.PLAYER, "/player.png");

		public static readonly EntityType ZOMBIE = new EntityType(InnerEnum.ZOMBIE, "/zombie.png");

		public static readonly EntityType WEAPON = new EntityType(InnerEnum.WEAPON, "");

		public static readonly EntityType BULLET = new EntityType(InnerEnum.BULLET, "/bullet.png");

		public static readonly EntityType MELEE_ATTACK = new EntityType(InnerEnum.MELEE_ATTACK, "/knifeAttack.png");

		public static readonly EntityType OBSTACLE = new EntityType(InnerEnum.OBSTACLE, "");

		private static readonly List<EntityType> valueList = new List<EntityType>();

		static EntityType()
		{
			valueList.Add(PLAYER);
			valueList.Add(ZOMBIE);
			valueList.Add(WEAPON);
			valueList.Add(BULLET);
			valueList.Add(MELEE_ATTACK);
			valueList.Add(OBSTACLE);
		}

		public enum InnerEnum
		{
			PLAYER,
			ZOMBIE,
			WEAPON,
			BULLET,
			MELEE_ATTACK,
			OBSTACLE
		}

		public readonly InnerEnum innerEnum;
		private readonly string nameValue;
		private readonly string url;

		internal EntityType(InnerEnum innerEnum, string url)
		{
			this.url = url;
			this.innerEnum = innerEnum;
		}

		public string URL
		{
			get
			{
				return this.url;
			}
		}

	}
}
