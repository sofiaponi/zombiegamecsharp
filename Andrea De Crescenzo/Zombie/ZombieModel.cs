﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Zombies
{
    public class ZombieModel : IZombieModel
    {

        private ISet<Zombie> zombies;
        private Tiers zombieTier;
        private int zombiesToSpawn;
        private ISet<Zombie> zombiesToKill { get; set; }
        private int killedZombies;

        public ZombieModel()
        {
            this.zombies = new HashSet<Zombie>();
            this.zombieTier = Tiers.WEAK;
            this.zombiesToKill = new HashSet<Zombie>();
            this.killedZombies = 0;
        }

        public void Update()
        {
            this.UpdateSpawns();
            this.UpdateKills();
        }

        public int ZombiesToSpawn
        {
            set
            {
                this.zombiesToSpawn = value;
            }
        }

        public Tiers ZombieTier
        {
            set
            {
                this.zombieTier = value;
            }
        }

        public ISet<Zombie> Zombies
        {
            get
            {
                return this.zombies;
            }
        }

        public int ZombiesCount
        {
            get
            {
                return this.zombiesToSpawn + this.killedZombies;
            }
        }

        public int KilledZombies
        {
            get
            {
                return this.killedZombies;
            }
        }

        public void HitZombie(Zombie zombie, int damage)
        {
            zombie.LifeManager.DecreaseHP(damage);
            if (!zombie.LifeManager.IsAlive())
            {
                this.KillZombie(zombie);
            }
        }

        private void KillZombie(Zombie zombie)
        {
            this.zombiesToKill.Add(zombie);
        }

        private void UpdateSpawns()
        {
            for (int i = 0; i < this.zombiesToSpawn; i++) { 
                this.SpawnZombie();
            }
        }

        private void SpawnZombie()
        {
            this.zombies.Add(new Zombie(new Point(0, 0), zombieTier.velocity, zombieTier.maxHp, zombieTier.damageDealt));
            this.zombiesToSpawn -= 1;
        }

        private void UpdateKills()
        {
            this.killedZombies += this.zombiesToKill.Count;
            foreach (Zombie zombie in this.zombiesToKill)
            {
                this.zombies.Remove(zombie);
            }
            this.zombiesToKill.Clear();
        }
    }
}
