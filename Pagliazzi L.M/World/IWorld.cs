﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace zombieversity
{
    interface IWorld
    {
        Dictionary<Point, Int32> GetBlocks();

       List<Obstacle> GetObstacles();
       List<ZombieSpawn> GetZombieSpawnPoints();
  
        PlayerSpawn GetPlayerSpawnPoint();

        void SetScale(double scale);
    }
}
