﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace zombieversity
{
    class World : IWorld
    {
        double tile_size { get; set; }
        double width { get; set; }
        double height { get; set; }
        double scale { get; set; }

        private Dictionary<Point, Int32> blocks;
        private List<Obstacle> objs = new List<Obstacle>();
        private List<ZombieSpawn> zSpawn = new List<ZombieSpawn>();
        private PlayerSpawn pSpawn;

        public World(Dictionary<Point, Int32> blocks, double width, double height, double tile_size)
        {
            this.blocks = blocks;
            this.width = width;
            this.height = height;
            this.tile_size = tile_size;
            this.scale = 1.0;
        }

        private void LoadObjs()
        {
            foreach( KeyValuePair<Point,Int32> block in blocks)
            {
                if(block.Value == Obstacle.id)
                {
                    objs.Add(new Obstacle());
                }
                if(block.Value == ZombieSpawn.id)
                {
                    zSpawn.Add(new ZombieSpawn());
                }
                if(block.Value == PlayerSpawn.id)
                {
                    pSpawn = new PlayerSpawn();
                }
            }
        }
        public Dictionary<Point, int> GetBlocks()
        {
            return this.blocks;
        }


        public List<Obstacle> GetObstacles()
        {
            return this.objs;
        }

        public PlayerSpawn GetPlayerSpawnPoint()
        {
            return this.pSpawn;
        }

        public List<ZombieSpawn> GetZombieSpawnPoints()
        {
            return this.zSpawn;
        }

        public void SetScale(double scale)
        {
            this.scale = scale;
        }
    }
}
