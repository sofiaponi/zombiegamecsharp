﻿namespace zombieversity
{
    internal class TileFactory
    {
        private string url;
        private double tile_size;

        public TileFactory(string url, double tile_size)
        {
            this.url = url;
            this.tile_size = tile_size;
        }
    }
}