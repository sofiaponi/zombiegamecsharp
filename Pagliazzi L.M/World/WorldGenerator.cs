﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using zombieversity;

namespace zombieversity
{
    class WorldGenerator : IWorldGenerator
    {
        private static int BACKGROUND_COUNT = 18;
        private TileFactory tile_factory;

        private Random rand = new Random();
        private static World generateWorld(Dictionary<Point, Int32> blocks, double width, double height, double tile_size)
        {
            return new World(blocks, width, height, tile_size);
        }
        private static WorldView generateWorldView(Dictionary<Point, Int32> blocks, TileFactory tile_factory,
        double tile_size, Int32 background, double width, double height)
        {
            return new WorldView(blocks, tile_factory, tile_size, background, width, height);
        }
        private static Dictionary<Point, Int32> GenerateEmpty()
        {
            return new Dictionary<Point, Int32>();
        }
        private static int GenerateObject()
        {
            return new Random().Next();
        }
        private Dictionary<Point, Int32> Generate(double width, double height, int background)
        {
            Point center = new Point((int) Math.Floor(width / 2),(int) Math.Floor(height / 2));
            Dictionary<Point, Int32> blocks = GenerateEmpty();
            FillBorder(blocks, Obstacle.id, width, height); // World Limit
            FillCell(blocks, PlayerSpawn.id, center); // Player Spawn
            FillAngles(blocks, ZombieSpawn.id, center, width - 2, height - 2); // Zombie spawns
            FillNextTo(blocks, background, center); // walkable area next to Player Spawn
            return blocks;
        }
        public WorldGenerator(String url, double tile_size)
        {
            this.tile_factory = new TileFactory(url, tile_size);
        }
        public void FillAngles(Dictionary<Point, int> blocks, int id, Point center, double width, double height)
        {
            Point pLURD = new Point((int) Math.Floor(width / 2), (int) Math.Floor(height / 2));
            Point pLD = new Point(pLURD.X* -1, pLURD.Y);
            Point pRU = new Point(pLURD.X, pLURD.Y * -1);
            // LEFT_UP
            FillCell(blocks, id, new Point(center.X + pLURD.X * (-1),center.Y + pLURD.Y * -1));
            // RIGHT_UP
            FillCell(blocks, id, new Point(center.X + pLD.X, center.Y + pLD.Y));
            // RIGHT_DOWN
            FillCell(blocks, id, new Point(center.X + pLURD.X, center.Y + pLURD.Y));
            // LEFT_DOWN
            FillCell(blocks, id, new Point(center.X + pRU.X, center.Y + pRU.Y));
        }

        public void FillBorder(Dictionary<Point, int> blocks, int id, double width, double height)
        {
            FillAngles(blocks, id, new Point((int) Math.Floor(width / 2), (int) Math.Floor(height / 2)), width, height);
            FillRow(blocks, id, new Point(1, 0), (int)width);
            FillRow(blocks, id, new Point(1, (int) height - 1), (int)width);
            FillColumn(blocks, id, new Point(0, 1), (int)height);
            FillColumn(blocks, id, new Point((int) width - 1, 1), (int)height);
        }

        public void FillCell(Dictionary<Point, int> blocks, int id, Point p)
        {
            if (!blocks.ContainsKey(p))
            {
                blocks.Add(p, id);
            }
        }

        public void FillColumn(Dictionary<Point, int> blocks, int id, Point initP, int count)
        {
            Point point = new Point(0, 1);
            for(int i = 0; i< count; i++)
            {
                FillCell(blocks, id, new Point(initP.X + point.X * i , initP.Y + point.Y * i));
            }
        }

        public void FillNextTo(Dictionary<Point, int> blocks, int id, Point point)
        {
            blocks.Add(new Point(-1 + point.X, -1 + point.Y ), id);
            blocks.Add(new Point(0 + point.X, -1 + point.Y), id);
            blocks.Add(new Point(1 + point.X, -1 + point.Y), id);
            // CENTER
            blocks.Add(new Point(-1 + point.X, 0 + point.Y), id);
            blocks.Add(new Point(1 + point.X, 0 + point.Y), id);
            // DOWN
            blocks.Add(new Point(-1 + point.X, 1 + point.Y), id);
            blocks.Add(new Point(0 + point.X, 1 + point.Y), id);
            blocks.Add(new Point(1 + point.X, 1 + point.Y), id);
        }

        public void FillRow(Dictionary<Point, int> blocks, int id, Point initP, int count)
        {
            Point point = new Point(1, 0);
            for (int i = 0; i < count; i++)
            {
                FillCell(blocks, id, new Point(initP.X + point.X * i, initP.Y + point.Y * i));
            }
        }

        public void GenerateRandom(Dictionary<Point, Int32> blocks, int rBound, int width, int height)
        {
            int count = rand.Next(rBound);
            for (int i = 0; i < count; i++)
            {
                FillCell(blocks, GenerateObject(), new Point(rand.Next(width), rand.Next(height)));
            }
        }

        public KeyValuePair<World, WorldView> Generate(double width, double height, double tileSize, int difficulty)
        {
            int background = GenerateBackground();
            Dictionary<Point, Int32> blocks = Generate(width, height, background);
            int bound = (int)((int)width * height / 100);
            GenerateRandom(blocks, (int) ( bound * 100 / difficulty), (int) width, (int) height);
            return new KeyValuePair<World, WorldView>(generateWorld(blocks, width, height, tileSize), generateWorldView(blocks, tile_factory, tileSize, background, width, height));
        }

        private int GenerateBackground()
        {
            return new Random().Next(BACKGROUND_COUNT);
        }

    }
}
