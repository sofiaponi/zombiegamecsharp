﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
namespace zombieversity
{
    interface ITileSet
    {
        void LoadImage(string url);

        void LoadTiles(int size);

        Image GetTile(int index);
        
    }
}
