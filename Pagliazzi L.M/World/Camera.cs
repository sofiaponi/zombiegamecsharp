﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace zombieversity
{
    class Camera : ICamera
    {
        private double xOffset { get; set; }
        private double yOffset { get; set; }
        private double map_width { get; set; }
        private double map_height { get; set; }
        private double camera_width { get; set; }
        private double camera_height { get; set; }

        public Camera(double map_width, double map_height, double camera_width, double camera_height, int x = 0, int y = 0  )
        {
            xOffset = x;
            yOffset = y;
            this.map_width = map_width;
            this.map_height = map_height;
            this.camera_width = camera_width;
            this.camera_height = camera_height;
        }
        public void CenterOn(double x, double y)
        {
            xOffset = x - camera_width / 2;
            yOffset = y - camera_height / 2;
            this.adjust();
        }

        public void CenterOnEntity(IEntity entity)
        {
            Point entity_pos = entity.GetPosition();
            this.CenterOn(entity_pos.X - (entity.GetWidth() / 2), entity_pos.Y - (entity.GetHeight() / 2));
        }

        public Point GetCenter()
        {
            return new Point((int) Math.Floor(map_width / 2), (int) Math.Floor(map_height / 2));
        }

        public Point GetEnd()
        {
            return new Point((int) (xOffset + camera_width),(int) ( yOffset + camera_height));
        }

        public Point GetOffset()
        {
            return new Point((int)xOffset, (int)yOffset);
        }

        public Point GetStart()
        {
            return this.GetOffset();
        }

        public Point MidPoint()
        {
            return new Point((int) (xOffset + camera_width / 2) , (int) ( yOffset + camera_height / 2));
        }
        private void adjust()
        {
            xOffset = xOffset + camera_width <= map_width? xOffset : map_width- camera_width;
            xOffset = xOffset >= 0 ? xOffset : 0;
            yOffset = yOffset + camera_height <= map_height ? yOffset : map_height - camera_height;
            yOffset = yOffset >= 0 ? yOffset : 0;
        }
        public void Move(Point step)
        {
            this.xOffset += step.X;
            this.yOffset += step.Y;
            this.adjust();
        }

        public void Move(double x, double y)
        {
            this.Move(new Point((int) x, (int) y));
        }

        public void resize(double width, double height)
        {
        }

        public void SetScale(double scale)
        {
        }
    }
}
