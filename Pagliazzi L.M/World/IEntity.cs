﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace zombieversity
{
    interface IEntity
    {

        Point GetPosition();

        double GetWidth();
        double GetHeight();
    }
}
