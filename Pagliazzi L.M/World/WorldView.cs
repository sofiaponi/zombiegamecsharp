﻿using System.Collections.Generic;
using System.Drawing;

namespace zombieversity
{
     class WorldView
    {
        private Dictionary<Point, int> blocks;
        private TileFactory tile_factory;
        private double tile_size;
        private int background;
        private double width;
        private double height;

        public WorldView(Dictionary<Point, int> blocks, TileFactory tile_factory, double tile_size, int background, double width, double height)
        {
            this.blocks = blocks;
            this.tile_factory = tile_factory;
            this.tile_size = tile_size;
            this.background = background;
            this.width = width;
            this.height = height;
        }
    }
}