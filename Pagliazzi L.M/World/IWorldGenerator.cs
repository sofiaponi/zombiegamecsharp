﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace zombieversity
{
    interface IWorldGenerator
    {

        public KeyValuePair<World, WorldView> Generate(double width, double height, double tileSize, int difficulty);
        public void GenerateRandom(Dictionary<Point, Int32> blocks, int rBound, int width, int height);
        public void FillNextTo(Dictionary<Point, Int32> blocks, int id, Point point);
        public void FillBorder(Dictionary<Point, Int32> blocks, int id, double width, double height);
        public void FillRow(Dictionary<Point, Int32> blocks, int id, Point initP, int count);
        public  void FillColumn(Dictionary<Point, Int32> blocks, int id, Point initP, int count);
        public void FillCell(Dictionary<Point, Int32> blocks, int id, Point p);
        public void FillAngles(Dictionary<Point, Int32> blocks, int id, Point center, double width, double height);

    } }

