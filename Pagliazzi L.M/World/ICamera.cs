﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace zombieversity
{
    interface ICamera
    {

        void Move(Point step);

        void Move(double x, double y);

        Point GetCenter();

        void CenterOnEntity(IEntity entity);
        void CenterOn(double x, double y);
        Point GetOffset();

        void SetScale(double scale);

        void resize(double width, double height);
        Point MidPoint();

        Point GetStart();
        Point GetEnd();



    }
}
