﻿using System.Drawing;

namespace Player
{
    public interface IPlayer : IActiveLivingEntity
    {

		void HitPlayer(int damage);

		void ComputeAngle(Point pos);
	}
}
