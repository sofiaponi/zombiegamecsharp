﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Player
{
    public sealed class PlayerKeyboardInput
    {
		private static Point point;
		private PlayerKeyboardInput()
		{
		}

		public static Point calculateVelocity(HashSet<Keys> keys)
		{
			point = new Point(0,0);

			foreach (Keys key in keys)
            {
				if(key.Equals(Keys.A))
				{
					
					point = Point.Add(point, new Size(-1,0));
				}
				if (key.Equals(Keys.D))
				{
					point = Point.Add(point, new Size(1, 0));
				}
				if (key.Equals(Keys.W))
				{
					point = Point.Add(point, new Size(0, -1));
				}
				if (key.Equals(Keys.S))
				{
					point = Point.Add(point, new Size(0, 1));
				}
			}
			return point;
		}
	}
}

