﻿namespace Player
{
    public interface IActiveLivingEntity : IActiveEntity
    {
        ILife LifeManager { get; }
    }
}
