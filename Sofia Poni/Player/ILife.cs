﻿namespace Player
{
    public interface ILife
    {
		int HP { get; }

		void DecreaseHP(int decrease);

		bool Alive { get; }
	}
}