﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    public class EntityType
    {
        	/// <summary>
		/// Player.
		/// </summary>
		public static readonly EntityType PLAYER = new EntityType("PLAYER", InnerEnum.PLAYER, "/player.png");
		/// <summary>
		/// Zombie.
		/// </summary>
		public static readonly EntityType ZOMBIE = new EntityType("ZOMBIE", InnerEnum.ZOMBIE, "/zombie.png");
		/// <summary>
		/// Weapon.
		/// </summary>
		public static readonly EntityType WEAPON = new EntityType("WEAPON", InnerEnum.WEAPON, "");
		/// <summary>
		/// Bullet.
		/// </summary>
		public static readonly EntityType BULLET = new EntityType("BULLET", InnerEnum.BULLET, "/bullet.png");
		/// 
		public static readonly EntityType MELEE_ATTACK = new EntityType("MELEE_ATTACK", InnerEnum.MELEE_ATTACK, "/knifeAttack.png");
		/// <summary>
		/// Obstacle.
		/// </summary>
		public static readonly EntityType OBSTACLE = new EntityType("OBSTACLE", InnerEnum.OBSTACLE, "");

		private static readonly List<EntityType> valueList = new List<EntityType>();

		static EntityType()
		{
			valueList.Add(PLAYER);
			valueList.Add(ZOMBIE);
			valueList.Add(WEAPON);
			valueList.Add(BULLET);
			valueList.Add(MELEE_ATTACK);
			valueList.Add(OBSTACLE);
		}

		public enum InnerEnum
		{
            PLAYER,
			ZOMBIE,
			WEAPON,
			BULLET,
			MELEE_ATTACK,
			OBSTACLE
		}

		public readonly InnerEnum innerEnumValue;
		private readonly string nameValue;
		private readonly string url;

		internal EntityType(string name, InnerEnum innerEnum, string url)
		{
            this.url = url;
			nameValue = name;
			innerEnumValue = innerEnum;
		}

		public string URL
		{
			get
			{
				return this.url;
			}
        }

		public override string ToString()
		{
			return nameValue;
		}

		public static EntityType valueOf(string name)
		{
			foreach (EntityType enumInstance in EntityType.valueList)
			{
				if (enumInstance.nameValue == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}
}