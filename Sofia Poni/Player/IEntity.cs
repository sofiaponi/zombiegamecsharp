﻿using System.Drawing;

namespace Player
{
    public interface IEntity
    {
		Point Position { get; set; }

		//BoundingBox BBox { get; }

		//void setBBox(double w, double h);

		EntityType Type { get; }

		double Width { get; }

		double Height { get; }
	}
}
