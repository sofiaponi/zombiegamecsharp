﻿using System;
using System.Drawing;

namespace Player
{
    public class PlayerImpl : AbstractActiveLivingEntity, IPlayer
    {
        private const int MAX_HP = 100;

        public PlayerImpl() : base(new Point(0, 0),
                                   MAX_HP,
                                   new Point(0, 0),
                                   EntityType.PLAYER) { }

        public virtual void HitPlayer(int damage)
        {
            LifeManager.DecreaseHP(damage);
        }

        public void ComputeAngle(Point pos)
        {
            Point disMov = Point.Subtract(Position, new Size((int)Width / 2, (int)Height / 2));
            double angle = Math.Tan(Math.Atan2(disMov.X, disMov.Y));

            Direction = angle;
        }
    }
}
