﻿using System.Drawing;

namespace Player
{
    public interface IActiveEntity : IEntity
    {
		Point Velocity { set; get; }


		void Update();

		double Direction { get; set; }
	}
}
