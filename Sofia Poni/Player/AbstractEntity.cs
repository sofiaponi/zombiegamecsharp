﻿using System.Drawing;

namespace Player
{
    public class AbstractEntity : IEntity
    {
		private Point position;
		private readonly EntityType type;
		private readonly double width;
		private readonly double height;

		public AbstractEntity(Point p2d, EntityType type)
		{
			this.position = p2d;
			this.type = type;
		}

		public virtual Point Position
		{
			get
			{
				return this.position;
			}
			set
			{
				this.position = value;
			}
		}

		/*public BoundingBox BBox
		{
			get
			{
				return new BoundingBox(this.Position.X, this.Position.Y, this.width, this.height);
			}
		}*/

		/*public void setBBox(double width, double height)
		{
			this.width = width;
			this.height = height;
		}*/

		public EntityType Type
		{
			get
			{
				return this.type;
			}
		}

		public virtual double Width
		{
			get
			{
				return this.width;
			}
		}

		public virtual double Height
		{
			get
			{
				return this.height;
			}
		}
    }
}
