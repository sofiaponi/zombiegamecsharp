﻿namespace Player
{
    public class LifeImpl : ILife
    {
		private int hp;
		public LifeImpl(int maxHp)
		{
			this.hp = maxHp;
		}

		public int HP
		{
			get
			{
				return this.hp;
			}
		}

		public void DecreaseHP(int damage)
		{
			this.hp -= damage;
		}

		public bool Alive
		{
			get
			{
				return this.hp > 0;
			}
		}

	}
}
