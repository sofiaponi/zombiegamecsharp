﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Player.Tests
{
    [TestClass()]
    public class PlayerImplTests
    {
        private IPlayer p = new PlayerImpl();
        private static readonly Point PLAYER_POSITION = new Point(10, 10);
        private const int MAX_HP = 100;
        private HashSet<Keys> keys = new HashSet<Keys>();

        [TestMethod()]
        public void HitPlayerTest()
        {
            p.LifeManager.DecreaseHP(1);
            Assert.AreEqual(p.LifeManager.HP, MAX_HP - 1);
        }

        [TestMethod()]
        public void PlayerMovementTest()
        {

            /*
             * Test of right movement of player
             */
            p.Position = PLAYER_POSITION;
            Point rightPosition = new Point(PLAYER_POSITION.X + 1, PLAYER_POSITION.Y);
            keys.Clear();
            keys.Add(Keys.D);
            p.Velocity = (PlayerKeyboardInput.calculateVelocity(keys));
            p.Update();
            Assert.AreEqual(rightPosition, p.Position);

            /*
             * Test of left movement of player
             */
            p.Position = PLAYER_POSITION;
            Point leftPosition = new Point(PLAYER_POSITION.X - 1, PLAYER_POSITION.Y);
            keys.Clear();
            keys.Add(Keys.A);
            p.Velocity = (PlayerKeyboardInput.calculateVelocity(keys));
            p.Update();
            Assert.AreEqual(leftPosition, p.Position);

            /*
             * Test of up movement of player
             */
            p.Position = PLAYER_POSITION;
            Point upPosition = new Point(PLAYER_POSITION.X, PLAYER_POSITION.Y - 1);
            keys.Clear();
            keys.Add(Keys.W);
            p.Velocity = (PlayerKeyboardInput.calculateVelocity(keys));
            p.Update();
            Assert.AreEqual(upPosition, p.Position);

            /*
             * Test of down movement of player
             */
            p.Position = PLAYER_POSITION;
            Point downPosition = new Point(PLAYER_POSITION.X, PLAYER_POSITION.Y + 1);
            keys.Clear();
            keys.Add(Keys.S);
            p.Velocity = (PlayerKeyboardInput.calculateVelocity(keys));
            p.Update();
            Assert.AreEqual(downPosition, p.Position);


            /*
             * Test multiple movement
             */
            keys.Clear();
            keys.Add(Keys.W);
            keys.Add(Keys.W);
            keys.Add(Keys.S);
            keys.Add(Keys.A);
            Point newPosition = new Point(PLAYER_POSITION.X - 1, PLAYER_POSITION.Y + 1);
            p.Velocity = (PlayerKeyboardInput.calculateVelocity(keys));
            p.Update();
            Assert.AreEqual(newPosition, p.Position);
        }
    }
}